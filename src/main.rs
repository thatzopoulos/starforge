mod aabb;
mod bvh;
mod camera;
mod cylinder;
mod hittable;
mod material;
mod perlin;
mod quad;
mod ray;
mod texture;
mod torus;
mod triangle;
mod vec;
use crate::torus::Torus;
use camera::Camera;
use cylinder::Cylinder;
use hittable::{obj_to_hittable, Hittable, HittableList, Sphere};
use log::{debug, error, info};
use mat::Mat3;
use material::Material;
use moving_sphere::MovingSphere;
use quad::Quad;
use rand::Rng;
use ray::Ray;
use rayon::prelude::*;
use std::{f64::consts::PI, fs, path::Path, sync::Arc};
use structopt::StructOpt;
use texture::{CheckerTexture, ImageTexture, NoiseTexture, SolidColor};
use tobj::{LoadOptions, Model};
use triangle::Triangle;
use vec::Vec3;
mod moving_sphere;
use env_logger::Env;
pub mod mat;

#[derive(Debug, StructOpt)]
#[structopt(name = "raytracer", about = "An argparser for the raytracer.")]
struct Opt {
    /// Output file, stdout if not present
    #[structopt(short = "o", long = "output", default_value = "default_name")]
    output: String,
    //width
    #[structopt(short = "w", long = "width", default_value = "800")]
    width: u32,
    //height
    #[structopt(short = "h", long = "height", default_value = "600")]
    height: u32,
    //nsamples
    #[structopt(short = "s", long = "samples", default_value = "10")]
    samples: u32,

    #[structopt(long = "obj-model")]
    objmodel: Option<String>,

    /// Creates a series of images that can be combined into a gif
    #[structopt(short, long)]
    gif: bool,

    /// Scene to render
    #[structopt(long = "scene", default_value = "random")]
    scene: String,

    /// Path to OBJ file (for obj_to_hittable and load_glass_obj_with_spheres scenes)
    #[structopt(long = "obj-path")]
    obj_path: Option<String>,
}

fn color(r: &Ray, world: &dyn Hittable, depth: u32) -> Vec3 {
    if depth >= 50 {
        return Vec3::new(0.0, 0.0, 0.0);
    }

    match world.hit(r, 0.001, std::f64::INFINITY) {
        Some((hit_record, material)) => match material.scatter(r, &hit_record) {
            Some((scattered, attenuation)) => attenuation * color(&scattered, world, depth + 1),
            None => Vec3::new(0.0, 0.0, 0.0),
        },
        None => {
            let unit_direction = r.direction().unit();
            let t = 0.5 * (unit_direction.y + 1.0);
            Vec3::new(1.0, 1.0, 1.0) * (1.0 - t) + Vec3::new(0.5, 0.7, 1.0) * t
        }
    }
}

/// Creates a scene with 3 large spheres in set locations,
/// surrounded by randomly placed tiny spheres
fn random_scene() -> HittableList {
    let mut rng = rand::thread_rng();
    let mut list: Vec<Arc<dyn Hittable>> = vec![Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: Arc::new(SolidColor::new(rng.gen(), rng.gen(), rng.gen())),
        },
    ))];
    for a in -11..11 {
        for b in -11..1 {
            let choose_mat = rng.gen::<f64>();
            let center = Vec3::new(
                f64::from(a) + 0.9 * rng.gen::<f64>(),
                0.2,
                f64::from(b) + 0.9 * rng.gen::<f64>(),
            );
            if (center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.8 {
                    // diffuse
                    let center2 =
                        center + Vec3::new(0.0, rand::thread_rng().gen_range(0.0..0.5), 0.0);

                    list.push(Arc::new(MovingSphere::new(
                        center,
                        center2,
                        0.0,
                        1.0,
                        0.2,
                        Material::Lambertian {
                            texture: Arc::new(SolidColor::new(rng.gen(), rng.gen(), rng.gen())),
                        },
                    )));
                } else if choose_mat < 0.95 {
                    // metal
                    list.push(Arc::new(Sphere::new(
                        center,
                        0.2,
                        Material::Metal {
                            attenuation: Vec3::new(
                                0.5 * (1.0 + rng.gen::<f64>()),
                                0.5 * (1.0 + rng.gen::<f64>()),
                                0.5 * (1.0 + rng.gen::<f64>()),
                            ),
                            fuzziness: 0.5 * rng.gen::<f64>(),
                        },
                    )));
                } else {
                    // glass
                    list.push(Arc::new(Sphere::new(
                        center,
                        0.2,
                        Material::Dielectric { refraction: 1.5 },
                    )));
                }
            }
        }
    }
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        Material::Dielectric { refraction: 1.5 },
    )));
    list.push(Arc::new(Sphere::new(
        Vec3::new(-4.0, 1.0, 0.0),
        1.0,
        Material::Lambertian {
            texture: Arc::new(SolidColor::new(rng.gen(), rng.gen(), rng.gen())),
        },
    )));
    list.push(Arc::new(Sphere::new(
        Vec3::new(4.0, 1.0, 0.0),
        1.0,
        Material::Metal {
            attenuation: Vec3::new(0.7, 0.6, 0.5),
            fuzziness: 0.0,
        },
    )));
    HittableList::new(list)
}

/// Creates a scene with 3 large spheres next to each other.
/// Returns: HittableList containing: 1 Lambertian, 1 Dielectric, and 1 Metal sphere
fn three_balls() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = vec![
        Arc::new(Sphere::new(
            Vec3::new(0.0, -1000.0, 0.0),
            1000.0,
            Material::Lambertian {
                texture: Arc::new(CheckerTexture::from_colors(
                    0.32,
                    Vec3::new(0.2, 0.3, 0.1),
                    Vec3::new(0.9, 0.9, 0.9),
                )),
            },
        )),
        Arc::new(Sphere::new(
            Vec3::new(0.0, 1.0, 0.0),
            1.0,
            Material::Dielectric { refraction: 1.5 },
        )),
        Arc::new(Sphere::new(
            Vec3::new(-4.0, 1.0, 0.0),
            1.0,
            Material::Lambertian {
                texture: Arc::new(SolidColor::new(0.4, 0.2, 0.1)),
            },
        )),
        Arc::new(Sphere::new(
            Vec3::new(4.0, 1.0, 0.0),
            1.0,
            Material::Metal {
                attenuation: Vec3::new(0.7, 0.6, 0.5),
                fuzziness: 0.0,
            },
        )),
    ];

    HittableList::new(list)
}
fn three_cylinders() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = vec![
        // Ground sphere
        Arc::new(Sphere::new(
            Vec3::new(0.0, -1000.0, 0.0),
            1000.0,
            Material::Lambertian {
                texture: Arc::new(CheckerTexture::from_colors(
                    0.32,
                    Vec3::new(0.2, 0.3, 0.1),
                    Vec3::new(0.9, 0.9, 0.9),
                )),
            },
        )),
        // Center cylinder (vertical)
        Arc::new(Cylinder::new(
            Vec3::new(0.0, 1.0, 0.0),
            0.5,
            2.0,
            Vec3::new(0.0, 1.0, 0.0),
            Arc::new(Material::Dielectric { refraction: 1.5 }),
        )),
        // Left cylinder (tilted 45 degrees towards camera)
        Arc::new(Cylinder::new(
            Vec3::new(-4.0, 1.0, 0.0),
            0.5,
            2.0,
            Vec3::new(0.0, 1.0, -1.0).unit(),
            Arc::new(Material::Lambertian {
                texture: Arc::new(SolidColor::new(0.4, 0.2, 0.1)),
            }),
        )),
        // Right cylinder (horizontal, pointing right)
        Arc::new(Cylinder::new(
            Vec3::new(4.0, 1.0, 0.0),
            0.5,
            2.0,
            Vec3::new(1.0, 0.0, 0.0),
            Arc::new(Material::Metal {
                attenuation: Vec3::new(0.7, 0.6, 0.5),
                fuzziness: 0.0,
            }),
        )),
    ];

    HittableList::new(list)
}
fn main() {
    let opt = Opt::from_args();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let aspect_ratio = opt.width as f64 / opt.height as f64;
    let look_from = Vec3::new(13.0, 2.0, 3.0);
    let look_at = Vec3::new(0.0, 0.0, 0.0);
    let view_up = Vec3::new(0.0, 1.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;

    let camera = Camera::new(
        look_from,
        look_at,
        view_up,
        20.0,
        aspect_ratio,
        aperture,
        dist_to_focus,
        0.0,
        1.0,
    );

    let world = match opt.scene.as_str() {
        "random" => random_scene(),
        "three_balls" => three_balls(),
        "multi_shape_scene" => multi_shape_scene(),
        "random_multi_shape_scene" => random_multi_shape_scene(),
        "three_cylinders" => three_cylinders(),
        "earth_scene" => earth_scene(),
        "quads_scene" => quads_scene(),
        "perlin_spheres" => perlin_spheres(),
        "floating_pyramids" => floating_pyramids(),
        "floating_cubes" => floating_cubes(),
        "glass_sphere_and_colorful_pyramids" => glass_sphere_and_colorful_pyramids(),
        "obj_to_hittable" => {
            let path = Path::new(
                opt.obj_path
                    .as_ref()
                    .expect("OBJ path is required for obj_to_hittable scene"),
            );
            obj_to_hittable(path)
        }
        "load_glass_obj_with_spheres" => {
            let path = opt
                .obj_path
                .as_ref()
                .expect("OBJ path is required for load_glass_obj_with_spheres scene");
            load_glass_obj_with_spheres(path)
        }
        "checkered_spheres" => checkered_spheres(),
        _ => {
            error!("Unknown scene: {}", opt.scene);
            std::process::exit(1);
        }
    };

    info!(":: Building BVH");
    let bvh = bvh::BVHNode::new_with_list(&mut world.clone(), 0.0, 1.0);

    debug!(":: Rendering Scene");
    let pixels = render_scene(&camera, &bvh, opt.width, opt.height, opt.samples);

    debug!(":: Generating PPM File");
    let ppm = format!("P3\n{} {}\n255\n{}", opt.width, opt.height, pixels);
    if fs::write("image.ppm", ppm).is_err() {
        error!("Could not generate the PPM File");
    }

    debug!(":: Generating PNG");
    let img = image::open("image.ppm").unwrap();
    img.save(format!("{}.png", opt.output)).unwrap();
}

fn render_scene(
    camera: &Camera,
    world: &dyn Hittable,
    width: u32,
    height: u32,
    samples_per_pixel: u32,
) -> String {
    (0..height)
        .into_par_iter()
        .rev()
        .map(|j| {
            (0..width)
                .into_par_iter()
                .map(|i| {
                    let mut pixel_color = Vec3::new(0.0, 0.0, 0.0);
                    for _ in 0..samples_per_pixel {
                        let u = (i as f64 + rand::random::<f64>()) / (width - 1) as f64;
                        let v = (j as f64 + rand::random::<f64>()) / (height - 1) as f64;
                        let r = camera.get_ray(u, v);
                        pixel_color += color(&r, world, 0);
                    }
                    pixel_color /= samples_per_pixel as f64;
                    pixel_color = Vec3::new(
                        pixel_color.x.sqrt(),
                        pixel_color.y.sqrt(),
                        pixel_color.z.sqrt(),
                    );

                    let ir = (255.99 * pixel_color.x) as u8;
                    let ig = (255.99 * pixel_color.y) as u8;
                    let ib = (255.99 * pixel_color.z) as u8;
                    format!("{} {} {}\n", ir, ig, ib)
                })
                .collect::<Vec<String>>()
                .join("")
        })
        .collect::<Vec<String>>()
        .join("")
}
pub fn render_random(
    width: u32,
    height: u32,
    ray_per_pixel: u32,
    max_color: i32,
    camera: Camera,
) -> String {
    info!(":: Generating World");

    let mut world = random_scene(); // or three_balls(), depending on the scene you want

    // Build BVH
    info!(":: Building BVH");
    let bvh = bvh::BVHNode::new_with_list(&mut world, 0.0, 1.0); // Assuming time0 and time1 are 0.0 and 1.0
                                                                 // info!(":: BVH: {:#?}",bvh.debug_info());
                                                                 // Pixels variable for ppm generation
    (0..height)
        .into_par_iter()
        .rev()
        .map(|h| {
            (0..width)
                .into_par_iter()
                .map(|w| {
                    let mut rng = rand::thread_rng();
                    let mut col = Vec3::new(0.0, 0.0, 0.0);
                    for _ in 0..ray_per_pixel {
                        let u = (w as f64 + rng.gen::<f64>()) / width as f64;
                        let v = (h as f64 + rng.gen::<f64>()) / height as f64;
                        let r = camera.get_ray(u, v);
                        col += color(&r, &bvh, 0); // Use BVH for intersection tests
                    }
                    col /= ray_per_pixel as f64;
                    col = Vec3::new(col.x.sqrt(), col.y.sqrt(), col.z.sqrt());
                    let ir = (max_color as f64 * col.x) as usize;
                    let ig = (max_color as f64 * col.y) as usize;
                    let ib = (max_color as f64 * col.z) as usize;

                    format!("{} {} {}\n", ir, ig, ib)
                })
                .collect::<Vec<String>>()
                .join("")
        })
        .collect::<Vec<String>>()
        .join("")
}

pub fn render_gif() {
    todo!()
}

pub fn render_obj(
    width: f32,
    height: f32,
    ray_per_pixel: f32,
    max_color: i32,
    camera: Camera,
    obj_path: &Path,
) -> String {
    let world = obj_to_hittable(obj_path);
    (0..height as u32)
        .into_par_iter()
        .rev()
        .map(|h| {
            (0..width as u32)
                .into_par_iter()
                .map(|w| {
                    let mut rng = rand::thread_rng();
                    let mut col = Vec3::new(0.0, 0.0, 0.0);
                    for _ in 0..ray_per_pixel as u32 {
                        let u = (f64::from(w) + rng.gen::<f64>()) / f64::from(width);
                        let v = (f64::from(h) + rng.gen::<f64>()) / f64::from(height);
                        let r = &camera.get_ray(u, v);
                        col += color(r, &world, 0);
                    }
                    col /= f64::from(ray_per_pixel);
                    col = Vec3::new(col.x.sqrt(), col.y.sqrt(), col.z.sqrt());
                    let ir = (f64::from(max_color) * col.x) as usize;
                    let ig = (f64::from(max_color) * col.y) as usize;
                    let ib = (f64::from(max_color) * col.z) as usize;

                    format!("{} {} {}\n", ir, ig, ib)
                })
                .collect::<Vec<String>>()
                .join("")
        })
        .collect::<Vec<String>>()
        .join("")
}
/// Creates a scene with floating pyramids and a central large sphere.
fn floating_pyramids() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = Vec::new();

    // Large ground sphere with checker texture
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1005.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: Arc::new(CheckerTexture::from_colors(
                0.32,
                Vec3::new(0.2, 0.3, 0.1),
                Vec3::new(0.9, 0.9, 0.9),
            )),
        },
    )));

    // Central large glass sphere
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 4.0, 0.0),
        3.0,
        Material::Dielectric { refraction: 1.5 },
    )));

    // Floating pyramids
    for i in 0..4 {
        let base_height = 2.0;
        let apex = Vec3::new(i as f64 * 5.0, base_height + 3.0, 0.0);
        for j in 0..3 {
            let angle = 2.0 * std::f64::consts::PI / 3.0 * j as f64;
            let next_angle = 2.0 * std::f64::consts::PI / 3.0 * (j + 1) as f64;
            let base1 = Vec3::new(
                apex.x + base_height * angle.cos(),
                base_height,
                apex.z + base_height * angle.sin(),
            );
            let base2 = Vec3::new(
                apex.x + base_height * next_angle.cos(),
                base_height,
                apex.z + base_height * next_angle.sin(),
            );

            // Triangle sides with metal texture
            list.push(Arc::new(triangle::Triangle::new(
                apex,
                base1,
                base2,
                Material::Metal {
                    attenuation: Vec3::new(0.7, 0.6, 0.5),
                    fuzziness: 0.1,
                }
                .into(),
            )));
        }
    }

    HittableList::new(list)
}

/// Creates an updated scene with floating cubes made of quads and a ground plane.
fn floating_cubes() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = Vec::new();

    // Ground plane made of quads
    let ground_size = 30.0;
    let checker_size = 3.0;
    for i in -5..5 {
        for j in -5..5 {
            let x = i as f64 * checker_size;
            let z = j as f64 * checker_size;
            let color = if (i + j) % 2 == 0 {
                Vec3::new(0.2, 0.3, 0.1)
            } else {
                Vec3::new(0.9, 0.9, 0.9)
            };
            list.push(Arc::new(Quad::new(
                Vec3::new(x, 0.0, z),
                Vec3::new(x + checker_size, 0.0, z),
                Vec3::new(x + checker_size, 0.0, z + checker_size),
                Vec3::new(x, 0.0, z + checker_size),
                Arc::new(Material::Lambertian {
                    texture: Arc::new(CheckerTexture::from_colors(1.0, color, color)),
                }),
            )));
        }
    }

    // Floating cubes
    let cube_positions = [
        Vec3::new(0.0, 1.0, 0.0),
        Vec3::new(-4.0, 2.0, -4.0),
        Vec3::new(4.0, 3.0, -2.0),
        Vec3::new(-2.0, 1.5, 3.0),
        Vec3::new(3.0, 2.5, 4.0),
    ];

    for (i, &center) in cube_positions.iter().enumerate() {
        let size = 1.0 + (i as f64 * 0.2);
        let material: Arc<Material> = Arc::new(match i {
            0 => Material::Lambertian {
                texture: Arc::new(CheckerTexture::from_colors(
                    0.1,
                    Vec3::new(0.2, 0.3, 0.1),
                    Vec3::new(0.9, 0.9, 0.9),
                )),
            },
            1 => Material::Metal {
                attenuation: Vec3::new(0.7, 0.6, 0.5),
                fuzziness: 0.1,
            },
            2 => Material::Dielectric { refraction: 1.5 },
            3 => Material::Lambertian {
                texture: Arc::new(NoiseTexture::new(0.1)),
            },
            _ => Material::Metal {
                attenuation: Vec3::new(0.8, 0.8, 0.9),
                fuzziness: 0.01,
            },
        });

        // Create 6 faces of the cube
        for j in 0..6 {
            let (v1, v2, v3, v4) = match j {
                0 => (
                    // Front face
                    Vec3::new(center.x - size, center.y - size, center.z + size),
                    Vec3::new(center.x + size, center.y - size, center.z + size),
                    Vec3::new(center.x + size, center.y + size, center.z + size),
                    Vec3::new(center.x - size, center.y + size, center.z + size),
                ),
                1 => (
                    // Back face
                    Vec3::new(center.x + size, center.y - size, center.z - size),
                    Vec3::new(center.x - size, center.y - size, center.z - size),
                    Vec3::new(center.x - size, center.y + size, center.z - size),
                    Vec3::new(center.x + size, center.y + size, center.z - size),
                ),
                2 => (
                    // Left face
                    Vec3::new(center.x - size, center.y - size, center.z - size),
                    Vec3::new(center.x - size, center.y - size, center.z + size),
                    Vec3::new(center.x - size, center.y + size, center.z + size),
                    Vec3::new(center.x - size, center.y + size, center.z - size),
                ),
                3 => (
                    // Right face
                    Vec3::new(center.x + size, center.y - size, center.z + size),
                    Vec3::new(center.x + size, center.y - size, center.z - size),
                    Vec3::new(center.x + size, center.y + size, center.z - size),
                    Vec3::new(center.x + size, center.y + size, center.z + size),
                ),
                4 => (
                    // Top face
                    Vec3::new(center.x - size, center.y + size, center.z + size),
                    Vec3::new(center.x + size, center.y + size, center.z + size),
                    Vec3::new(center.x + size, center.y + size, center.z - size),
                    Vec3::new(center.x - size, center.y + size, center.z - size),
                ),
                _ => (
                    // Bottom face
                    Vec3::new(center.x - size, center.y - size, center.z - size),
                    Vec3::new(center.x + size, center.y - size, center.z - size),
                    Vec3::new(center.x + size, center.y - size, center.z + size),
                    Vec3::new(center.x - size, center.y - size, center.z + size),
                ),
            };

            list.push(Arc::new(Quad::new(v1, v2, v3, v4, Arc::clone(&material))));
        }
    }

    HittableList::new(list)
}
fn add_pyramid(list: &mut Vec<Arc<dyn Hittable>>, center: Vec3, size: f64, material: Material) {
    let half_size = size / 2.0;
    let height = size; // Adjust for desired pyramid height

    // Pyramid base vertices
    let p1 = Vec3::new(center.x - half_size, center.y, center.z - half_size);
    let p2 = Vec3::new(center.x + half_size, center.y, center.z - half_size);
    let p3 = Vec3::new(center.x + half_size, center.y, center.z + half_size);
    let p4 = Vec3::new(center.x - half_size, center.y, center.z + half_size);

    // Apex of the pyramid
    let apex = Vec3::new(center.x, center.y + height, center.z);

    let arc_material = Arc::new(material); // Wrap material in Arc

    // Base
    list.push(Arc::new(triangle::Triangle::new(
        p1,
        p2,
        p3,
        arc_material.clone(),
    )));
    list.push(Arc::new(triangle::Triangle::new(
        p1,
        p3,
        p4,
        arc_material.clone(),
    )));

    // Sides
    list.push(Arc::new(triangle::Triangle::new(
        p1,
        p2,
        apex,
        arc_material.clone(),
    )));
    list.push(Arc::new(triangle::Triangle::new(
        p2,
        p3,
        apex,
        arc_material.clone(),
    )));
    list.push(Arc::new(triangle::Triangle::new(
        p3,
        p4,
        apex,
        arc_material.clone(),
    )));
    list.push(Arc::new(triangle::Triangle::new(
        p4,
        p1,
        apex,
        arc_material,
    )));
}

fn glass_sphere_and_colorful_pyramids() -> HittableList {
    let mut rng = rand::thread_rng();
    let mut list: Vec<Arc<dyn Hittable>> = Vec::new();

    // Large ground sphere for the base with a checker texture
    let checker =
        CheckerTexture::from_colors(0.32, Vec3::new(0.2, 0.3, 0.1), Vec3::new(0.9, 0.9, 0.9));
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: Arc::new(checker),
        },
    )));

    // Central large glass sphere
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        Material::Dielectric { refraction: 1.5 },
    )));

    // Adding colorful pyramids
    for a in -3..3 {
        for b in -3..3 {
            let choose_mat = rng.gen::<f64>();
            let center = Vec3::new(a as f64 * 4.0, 0.0, b as f64 * 4.0);

            if choose_mat < 0.33 {
                // Lambertian pyramids with solid color texture
                let albedo = Vec3::new(rng.gen(), rng.gen(), rng.gen());
                let pyramid_material = Material::Lambertian {
                    texture: Arc::new(SolidColor::from_vec3(albedo)),
                };
                add_pyramid(&mut list, center, 1.0, pyramid_material);
            } else if choose_mat < 0.66 {
                // Metal pyramids
                let pyramid_material = Material::Metal {
                    attenuation: Vec3::new(
                        0.5 * (1.0 + rng.gen::<f64>()),
                        0.5 * (1.0 + rng.gen::<f64>()),
                        0.5 * (1.0 + rng.gen::<f64>()),
                    ),
                    fuzziness: 0.3 * rng.gen::<f64>(),
                };
                add_pyramid(&mut list, center, 1.0, pyramid_material);
            } else {
                // Glass pyramids
                let pyramid_material = Material::Dielectric { refraction: 1.5 };
                add_pyramid(&mut list, center, 1.0, pyramid_material);
            }
        }
    }

    HittableList::new(list)
}
fn calculate_bounding_box(models: &[Model]) -> (Vec3, Vec3) {
    let mut min_point = Vec3::new(f64::MAX, f64::MAX, f64::MAX);
    let mut max_point = Vec3::new(f64::MIN, f64::MIN, f64::MIN);

    for model in models {
        for i in 0..model.mesh.positions.len() / 3 {
            let x = model.mesh.positions[i * 3];
            let y = model.mesh.positions[i * 3 + 1];
            let z = model.mesh.positions[i * 3 + 2];
            min_point = Vec3::new(
                f64::min(min_point.x, x.into()),
                f64::min(min_point.y, y.into()),
                f64::min(min_point.z, z.into()),
            );
            max_point = Vec3::new(
                f64::max(max_point.x, x.into()),
                f64::max(max_point.y, y.into()),
                f64::max(max_point.z, z.into()),
            );
        }
    }

    (min_point, max_point)
}

pub fn setup_camera_for_obj(path: &Path, aspect_ratio: f64) -> Camera {
    // Load the OBJ file
    let obj = tobj::load_obj(&path, &LoadOptions::default()).unwrap();
    let (models, _) = obj;

    // Calculate the bounding box
    let (min_point, max_point) = calculate_bounding_box(&models);
    let center = (min_point + max_point) / 2.0;

    // Determine the size of the model and appropriate distance
    let bbox_size = max_point - min_point;
    let max_dimension = bbox_size.x.max(bbox_size.y).max(bbox_size.z);
    let fov: f64 = 40.0; // Field of view in degrees
    let distance = max_dimension / (2.0 * (fov / 2.0).to_radians().tan());

    // Set up camera
    let look_from = center + Vec3::new(0.0, 0.0, distance);
    let look_at = center;
    let view_up = Vec3::new(0.0, 1.0, 0.0);
    let dist_to_focus = (look_from - look_at).length();
    let aperture = 0.1;

    Camera::new(
        look_from,
        look_at,
        view_up,
        fov,
        aspect_ratio,
        aperture,
        dist_to_focus,
        0.0,
        1.0,
    )
}
fn load_glass_obj_with_spheres(obj_path: &str) -> HittableList {
    let mut world: Vec<Arc<dyn Hittable>> = Vec::new();

    // Load the OBJ file
    let path = Path::new(obj_path);
    let obj = tobj::load_obj(&path, &LoadOptions::default()).unwrap();
    let (models, _) = obj;

    // Glass material for the OBJ model
    let glass_material = Material::Dielectric { refraction: 1.5 };

    // Process each triangle in the OBJ model
    for m in models.iter() {
        let mesh = &m.mesh;
        for f in 0..mesh.indices.len() / 3 {
            let i0 = mesh.indices[3 * f] as usize;
            let i1 = mesh.indices[3 * f + 1] as usize;
            let i2 = mesh.indices[3 * f + 2] as usize;

            let tri = triangle::Triangle::new(
                Vec3::new(
                    mesh.positions[i0 * 3].into(),
                    mesh.positions[i0 * 3 + 1].into(),
                    mesh.positions[i0 * 3 + 2].into(),
                ),
                Vec3::new(
                    mesh.positions[i1 * 3].into(),
                    mesh.positions[i1 * 3 + 1].into(),
                    mesh.positions[i1 * 3 + 2].into(),
                ),
                Vec3::new(
                    mesh.positions[i2 * 3].into(),
                    mesh.positions[i2 * 3 + 1].into(),
                    mesh.positions[i2 * 3 + 2].into(),
                ),
                glass_material.clone().into(),
            );

            world.push(Arc::new(tri));
        }
    }

    // Add random spheres with various textures
    let mut rng = rand::thread_rng();
    for _ in 0..100 {
        let center = Vec3::new(
            rng.gen_range(-5.0..5.0),
            rng.gen_range(-5.0..5.0),
            rng.gen_range(-5.0..5.0),
        );
        let radius = rng.gen_range(0.1..1.0);
        let choose_mat = rng.gen::<f64>();

        let sphere_material = if choose_mat < 0.8 {
            // Lambertian with solid color texture
            Material::Lambertian {
                texture: Arc::new(SolidColor::new(rng.gen(), rng.gen(), rng.gen())),
            }
        } else if choose_mat < 0.95 {
            // Metal
            Material::Metal {
                attenuation: Vec3::new(
                    0.5 * (1.0 + rng.gen::<f64>()),
                    0.5 * (1.0 + rng.gen::<f64>()),
                    0.5 * (1.0 + rng.gen::<f64>()),
                ),
                fuzziness: rng.gen_range(0.0..0.5),
            }
        } else {
            // Glass
            Material::Dielectric { refraction: 1.5 }
        };

        world.push(Arc::new(Sphere::new(center, radius, sphere_material)));
    }

    // Add a large checker textured sphere for the ground
    world.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: Arc::new(CheckerTexture::from_colors(
                0.32,
                Vec3::new(0.2, 0.3, 0.1),
                Vec3::new(0.9, 0.9, 0.9),
            )),
        },
    )));

    HittableList::new(world)
}
fn checkered_spheres() -> HittableList {
    let mut world: Vec<Arc<dyn Hittable>> = Vec::new();

    let checker = Arc::new(CheckerTexture::from_colors(
        0.32,
        Vec3::new(0.2, 0.3, 0.1),
        Vec3::new(0.9, 0.9, 0.9),
    ));

    world.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -10.0, 0.0),
        10.0,
        Material::Lambertian {
            texture: checker.clone(),
        },
    )));

    world.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 10.0, 0.0),
        10.0,
        Material::Lambertian { texture: checker },
    )));

    HittableList::new(world)
}

fn earth_scene() -> HittableList {
    // Create the earth texture
    let earth_texture =
        Arc::new(ImageTexture::new("earthmap.jpg").expect("Failed to load earth texture"));

    // Create the earth surface material
    let earth_surface = Material::Lambertian {
        texture: earth_texture,
    };

    // Create the earth globe
    let globe = Arc::new(Sphere::new(
        Vec3::new(0.0, 0.0, 0.0), // center at origin
        2.0,                      // radius of 2
        earth_surface,
    ));

    // Create a list with just the globe
    let list: Vec<Arc<dyn Hittable>> = vec![globe];

    // Return the HittableList
    HittableList::new(list)
}
fn perlin_spheres() -> HittableList {
    let mut world = HittableList::new(Vec::new());

    let pertext = Arc::new(NoiseTexture::new(1.0));

    // Large sphere as the "ground"
    world.list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: pertext.clone(),
        },
    )));

    // Smaller sphere above the ground
    world.list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 2.0, 0.0),
        2.0,
        Material::Lambertian { texture: pertext },
    )));

    world
}

fn multi_shape_scene() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = vec![];

    // Ground plane (large checker-textured sphere)
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Material::Lambertian {
            texture: Arc::new(CheckerTexture::from_colors(
                0.32,
                Vec3::new(0.2, 0.3, 0.1),
                Vec3::new(0.9, 0.9, 0.9),
            )),
        },
    )));

    // Sphere
    list.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        Material::Dielectric { refraction: 1.5 },
    )));

    // Cylinder
    list.push(Arc::new(Cylinder::new(
        Vec3::new(-4.0, 1.0, 0.0),
        0.5,
        2.0,
        Vec3::new(0.0, 1.0, -1.0).unit(),
        Arc::new(Material::Lambertian {
            texture: Arc::new(SolidColor::new(0.4, 0.2, 0.1)),
        }),
    )));

    // // Triangle
    // list.push(Arc::new(Triangle::new(
    //     Vec3::new(4.0, 0.0, 0.0),
    //     Vec3::new(5.0, 2.0, 1.0),
    //     Vec3::new(3.0, 2.0, -1.0),
    //     Arc::new(Material::Metal {
    //         attenuation: Vec3::new(0.7, 0.6, 0.5),
    //         fuzziness: 0.0,
    //     }),
    // )));

    // Quad
    list.push(Arc::new(Quad::new(
        Vec3::new(-2.0, 0.0, -2.0),
        Vec3::new(0.0, 0.0, -2.0),
        Vec3::new(0.0, 2.0, -2.0),
        Vec3::new(-2.0, 2.0, -2.0),
        Arc::new(Material::Lambertian {
            texture: Arc::new(SolidColor::new(0.2, 0.8, 0.2)),
        }),
    )));

    // Torus
    list.push(Arc::new(Torus::new(
        Vec3::new(-2.0, 1.0, 2.0),
        1.0,
        0.25,
        Arc::new(Material::Lambertian {
            texture: Arc::new(SolidColor::new(0.7, 0.3, 0.3)),
        }),
        Mat3::rotation_y(std::f64::consts::PI / 4.0), // rotate 45 degrees around the Y-axis:
    )));

    HittableList::new(list)
}
fn random_multi_shape_scene() -> HittableList {
    let mut rng = rand::thread_rng();
    let mut world: Vec<Arc<dyn Hittable>> = Vec::new();

    // Ground
    let ground_material = Material::Lambertian {
        texture: Arc::new(CheckerTexture::from_colors(
            0.32,
            Vec3::new(0.2, 0.3, 0.1),
            Vec3::new(0.9, 0.9, 0.9),
        )),
    };
    world.push(Arc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        ground_material,
    )));

    // Generate random shapes
    for a in -11..11 {
        for b in -11..11 {
            let choose_mat: f64 = rng.gen();
            let center = Vec3::new(
                a as f64 + 0.9 * rng.gen::<f64>(),
                0.2,
                b as f64 + 0.9 * rng.gen::<f64>(),
            );

            if (center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                let shape_type: u8 = rng.gen_range(0..4); // 0: Sphere, 1: Cylinder, 2: Triangle, 3: Torus

                let material = if choose_mat < 0.8 {
                    // Diffuse
                    let albedo = random_vec3(&mut rng) * random_vec3(&mut rng);
                    Material::Lambertian {
                        texture: Arc::new(SolidColor::from_vec3(albedo)),
                    }
                } else if choose_mat < 0.95 {
                    // Metal
                    let albedo = random_vec3_range(&mut rng, 0.5, 1.0);
                    let fuzz = rng.gen_range(0.0..0.5);
                    Material::Metal {
                        attenuation: albedo,
                        fuzziness: fuzz,
                    }
                } else {
                    // Glass
                    Material::Dielectric { refraction: 1.5 }
                };

                let shape = create_random_shape(shape_type, center, material, &mut rng);
                world.push(shape);
            }
        }
    }

    // Add three large objects
    let material1 = Material::Dielectric { refraction: 1.5 };
    world.push(Arc::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        material1,
    )));

    let material2 = Material::Lambertian {
        texture: Arc::new(SolidColor::new(0.4, 0.2, 0.1)),
    };
    // rotate it randomly

    // Generate random angles between 0 and 2π for each axis
    let angle_x = rng.gen_range(0.0..2.0 * PI);
    let angle_y = rng.gen_range(0.0..2.0 * PI);
    let angle_z = rng.gen_range(0.0..2.0 * PI);

    // Create rotation matrices for each axis
    let rotation_x = Mat3::rotation_x(angle_x);
    let rotation_y = Mat3::rotation_y(angle_y);
    let rotation_z = Mat3::rotation_z(angle_z);
    let combined_rotation = rotation_x * rotation_y * rotation_z;
    world.push(Arc::new(Torus::new(
        Vec3::new(5.0, 1.0, 0.0),
        0.6,
        0.1,
        Arc::new(material2),
        combined_rotation,
    )));

    let material3 = Material::Metal {
        attenuation: Vec3::new(0.7, 0.6, 0.5),
        fuzziness: 0.0,
    };
    world.push(Arc::new(Cylinder::new(
        Vec3::new(-4.0, 0.0, 0.0),
        0.5,
        2.0,
        Vec3::new(0.0, 1.0, 0.0),
        Arc::new(material3),
    )));

    HittableList::new(world)
}

fn quads_scene() -> HittableList {
    let mut list: Vec<Arc<dyn Hittable>> = Vec::new();

    // Materials
    let left_red = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(1.0, 0.2, 0.2)),
    });
    let back_green = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(0.2, 1.0, 0.2)),
    });
    let right_blue = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(0.2, 0.2, 1.0)),
    });
    let upper_orange = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(1.0, 0.5, 0.0)),
    });
    let lower_teal = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(0.2, 0.8, 0.8)),
    });

    // Quads
    // Left red quad
    list.push(Arc::new(Quad::new(
        Vec3::new(-3.0, -2.0, 5.0),
        Vec3::new(-3.0, -2.0, 1.0),
        Vec3::new(-3.0, 2.0, 1.0),
        Vec3::new(-3.0, 2.0, 5.0),
        left_red,
    )));

    // Back green quad
    list.push(Arc::new(Quad::new(
        Vec3::new(-2.0, -2.0, 0.0),
        Vec3::new(2.0, -2.0, 0.0),
        Vec3::new(2.0, 2.0, 0.0),
        Vec3::new(-2.0, 2.0, 0.0),
        back_green,
    )));

    // Right blue quad
    list.push(Arc::new(Quad::new(
        Vec3::new(3.0, -2.0, 1.0),
        Vec3::new(3.0, -2.0, 5.0),
        Vec3::new(3.0, 2.0, 5.0),
        Vec3::new(3.0, 2.0, 1.0),
        right_blue,
    )));

    // Upper orange quad
    list.push(Arc::new(Quad::new(
        Vec3::new(-2.0, 3.0, 1.0),
        Vec3::new(2.0, 3.0, 1.0),
        Vec3::new(2.0, 3.0, 5.0),
        Vec3::new(-2.0, 3.0, 5.0),
        upper_orange,
    )));

    // Lower teal quad
    list.push(Arc::new(Quad::new(
        Vec3::new(-2.0, -3.0, 5.0),
        Vec3::new(2.0, -3.0, 5.0),
        Vec3::new(2.0, -3.0, 1.0),
        Vec3::new(-2.0, -3.0, 1.0),
        lower_teal,
    )));

    HittableList::new(list)
}

fn create_random_shape(
    shape_type: u8,
    center: Vec3,
    material: Material,
    rng: &mut impl Rng,
) -> Arc<dyn Hittable> {
    match shape_type {
        0 => Arc::new(Sphere::new(center, 0.2, material)),
        1 => Arc::new(Cylinder::new(
            center,
            0.1,
            0.4,
            random_unit_vector(rng),
            Arc::new(material),
        )),
        2 => {
            let v1 = center;
            let v2 = center + random_in_unit_sphere(rng) * 0.2;
            let v3 = center + random_in_unit_sphere(rng) * 0.2;
            Arc::new(Triangle::new(v1, v2, v3, Arc::new(material)))
        }
        3 => {
            let mut rng = rand::thread_rng();

            // Generate random angles between 0 and 2π for each axis
            let angle_x = rng.gen_range(0.0..2.0 * PI);
            let angle_y = rng.gen_range(0.0..2.0 * PI);
            let angle_z = rng.gen_range(0.0..2.0 * PI);

            // Create rotation matrices for each axis
            let rotation_x = Mat3::rotation_x(angle_x);
            let rotation_y = Mat3::rotation_y(angle_y);
            let rotation_z = Mat3::rotation_z(angle_z);

            // Combine rotations (order matters: we apply z, then y, then x rotation)
            let combined_rotation = rotation_x * rotation_y * rotation_z;

            Arc::new(Torus::new(
                center,
                0.2,
                0.05,
                Arc::new(material),
                combined_rotation,
            ))
        }
        _ => unreachable!(),
    }
}

// Helper functions for random vector generation
fn random_vec3(rng: &mut impl Rng) -> Vec3 {
    Vec3::new(rng.gen(), rng.gen(), rng.gen())
}

fn random_vec3_range(rng: &mut impl Rng, min: f64, max: f64) -> Vec3 {
    Vec3::new(
        rng.gen_range(min..max),
        rng.gen_range(min..max),
        rng.gen_range(min..max),
    )
}

fn random_in_unit_sphere(rng: &mut impl Rng) -> Vec3 {
    loop {
        let p = random_vec3_range(rng, -1.0, 1.0);
        if p.squared_length() < 1.0 {
            return p;
        }
    }
}

fn random_unit_vector(rng: &mut impl Rng) -> Vec3 {
    random_in_unit_sphere(rng).unit()
}
