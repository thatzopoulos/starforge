use crate::{
    aabb::AABB,
    hittable::{HitRecord, Hittable},
    material::Material,
    ray::Ray,
    vec::Vec3,
};
use std::sync::Arc;

pub struct Cylinder {
    center: Vec3,
    radius: f64,
    height: f64,
    axis: Vec3,
    material: Arc<Material>,
}

impl Cylinder {
    pub fn new(
        center: Vec3,
        radius: f64,
        height: f64,
        axis: Vec3,
        material: Arc<Material>,
    ) -> Self {
        Self {
            center,
            radius,
            height,
            axis: axis.unit(),
            material,
        }
    }
}

impl Hittable for Cylinder {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        let oc = r.origin() - self.center;
        let axis_dot_dir = self.axis.dot(r.direction());
        let axis_dot_oc = self.axis.dot(oc);

        let a = r.direction().squared_length() - axis_dot_dir.powi(2);
        let b = oc.dot(r.direction()) - axis_dot_oc * axis_dot_dir;
        let c = oc.squared_length() - axis_dot_oc.powi(2) - self.radius.powi(2);

        let discriminant = b * b - a * c;
        if discriminant < 0.0 {
            return None;
        }

        let sqrt_disc = discriminant.sqrt();
        let mut t = (-b - sqrt_disc) / a;
        if t < t_min || t > t_max {
            t = (-b + sqrt_disc) / a;
            if t < t_min || t > t_max {
                return None;
            }
        }

        let hit_point = r.point_at_parameter(t);
        let outward_normal =
            (hit_point - self.center - self.axis * self.axis.dot(hit_point - self.center))
                / self.radius;

        let half_height = self.height / 2.0;
        let hit_height = self.axis.dot(hit_point - self.center);
        if hit_height.abs() > half_height {
            // Check end caps
            let t_cap = (half_height * hit_height.signum() - axis_dot_oc) / axis_dot_dir;
            if t_cap < t_min || t_cap > t_max {
                return None;
            }
            let hit_point_cap = r.point_at_parameter(t_cap);
            let outward_normal_cap = self.axis * hit_height.signum();
            if (hit_point_cap - self.center - self.axis * half_height * hit_height.signum())
                .squared_length()
                > self.radius.powi(2)
            {
                return None;
            }
            let u =
                (hit_point_cap.x().atan2(hit_point_cap.z()) / (2.0 * std::f64::consts::PI)) + 0.5;
            let v = hit_point_cap.y() / self.height + 0.5;
            return Some((
                HitRecord::new(t_cap, hit_point_cap, outward_normal_cap, u, v),
                &*self.material,
            ));
        }

        let u = (hit_point.x().atan2(hit_point.z()) / (2.0 * std::f64::consts::PI)) + 0.5;
        let v = hit_height / self.height + 0.5;
        Some((
            HitRecord::new(t, hit_point, outward_normal, u, v),
            &*self.material,
        ))
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        let radius_vec = Vec3::new(self.radius, self.radius, self.radius);
        let half_height = self.height / 2.0;
        let min = self.center - radius_vec - self.axis * half_height;
        let max = self.center + radius_vec + self.axis * half_height;
        Some(AABB::new(min, max))
    }

    fn debug_info(&self) -> String {
        format!(
            "Cylinder: center = {:?}, radius = {}, height = {}, axis = {:?}",
            self.center, self.radius, self.height, self.axis
        )
    }
}
