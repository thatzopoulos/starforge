use crate::vec::Vec3;
use std::ops::Mul;

#[derive(Clone, Copy, Debug)]
pub struct Mat3 {
    elements: [[f64; 3]; 3],
}

impl Mat3 {
    pub fn new(elements: [[f64; 3]; 3]) -> Self {
        Mat3 { elements }
    }

    pub fn identity() -> Self {
        Mat3::new([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
    }

    pub fn rotation_x(angle: f64) -> Self {
        let cos = angle.cos();
        let sin = angle.sin();
        Mat3::new([[1.0, 0.0, 0.0], [0.0, cos, -sin], [0.0, sin, cos]])
    }

    pub fn rotation_y(angle: f64) -> Self {
        let cos = angle.cos();
        let sin = angle.sin();
        Mat3::new([[cos, 0.0, sin], [0.0, 1.0, 0.0], [-sin, 0.0, cos]])
    }

    pub fn rotation_z(angle: f64) -> Self {
        let cos = angle.cos();
        let sin = angle.sin();
        Mat3::new([[cos, -sin, 0.0], [sin, cos, 0.0], [0.0, 0.0, 1.0]])
    }

    pub fn inverse(&self) -> Self {
        let det = self.determinant();
        assert!(det.abs() > 1e-6, "Matrix is not invertible");

        let inv_det = 1.0 / det;
        let e = &self.elements;

        Mat3::new([
            [
                (e[1][1] * e[2][2] - e[1][2] * e[2][1]) * inv_det,
                (e[0][2] * e[2][1] - e[0][1] * e[2][2]) * inv_det,
                (e[0][1] * e[1][2] - e[0][2] * e[1][1]) * inv_det,
            ],
            [
                (e[1][2] * e[2][0] - e[1][0] * e[2][2]) * inv_det,
                (e[0][0] * e[2][2] - e[0][2] * e[2][0]) * inv_det,
                (e[0][2] * e[1][0] - e[0][0] * e[1][2]) * inv_det,
            ],
            [
                (e[1][0] * e[2][1] - e[1][1] * e[2][0]) * inv_det,
                (e[0][1] * e[2][0] - e[0][0] * e[2][1]) * inv_det,
                (e[0][0] * e[1][1] - e[0][1] * e[1][0]) * inv_det,
            ],
        ])
    }

    fn determinant(&self) -> f64 {
        let e = &self.elements;
        e[0][0] * (e[1][1] * e[2][2] - e[1][2] * e[2][1])
            - e[0][1] * (e[1][0] * e[2][2] - e[1][2] * e[2][0])
            + e[0][2] * (e[1][0] * e[2][1] - e[1][1] * e[2][0])
    }
}

impl Mul<Vec3> for Mat3 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Vec3 {
        Vec3::new(
            self.elements[0][0] * rhs.x()
                + self.elements[0][1] * rhs.y()
                + self.elements[0][2] * rhs.z(),
            self.elements[1][0] * rhs.x()
                + self.elements[1][1] * rhs.y()
                + self.elements[1][2] * rhs.z(),
            self.elements[2][0] * rhs.x()
                + self.elements[2][1] * rhs.y()
                + self.elements[2][2] * rhs.z(),
        )
    }
}

impl Mul for Mat3 {
    type Output = Mat3;

    fn mul(self, rhs: Mat3) -> Mat3 {
        let mut result = [[0.0; 3]; 3];
        for i in 0..3 {
            for j in 0..3 {
                for k in 0..3 {
                    result[i][j] += self.elements[i][k] * rhs.elements[k][j];
                }
            }
        }
        Mat3::new(result)
    }
}
