use crate::{
    aabb::AABB,
    hittable::{HitRecord, Hittable},
    mat::Mat3,
    material::Material,
    ray::Ray,
    vec::Vec3,
};
use std::sync::Arc;
pub struct Torus {
    center: Vec3,
    major_radius: f64,
    minor_radius: f64,
    material: Arc<Material>,
    rotation: Mat3,
    inv_rotation: Mat3,
}

impl Torus {
    pub fn new(
        center: Vec3,
        major_radius: f64,
        minor_radius: f64,
        material: Arc<Material>,
        rotation: Mat3,
    ) -> Self {
        let inv_rotation = rotation.inverse();
        Self {
            center,
            major_radius,
            minor_radius,
            material,
            rotation,
            inv_rotation,
        }
    }

    fn to_object_space(&self, p: &Vec3) -> Vec3 {
        self.inv_rotation * (*p - self.center)
    }

    fn to_world_space(&self, v: &Vec3) -> Vec3 {
        self.rotation * *v + self.center
    }
}

impl Hittable for Torus {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        // Transform the ray to object space
        let transformed_origin = self.to_object_space(&r.origin());
        let transformed_direction = self.inv_rotation * r.direction();
        let transformed_ray = Ray::new(transformed_origin, transformed_direction, 0.0); // todo figure out what third arg should be here

        let oc = transformed_ray.origin();
        let a = transformed_ray.direction().dot(transformed_ray.direction());
        let b = 2.0 * oc.dot(transformed_ray.direction());
        let c = oc.dot(oc) - self.major_radius.powi(2) - self.minor_radius.powi(2);
        let d = transformed_ray.direction().y();
        let e = oc.y();
        let f = self.major_radius.powi(2) - self.minor_radius.powi(2);

        // Solve quartic equation
        let coeffs = [
            a.powi(2),
            2.0 * a * b,
            2.0 * a * c + b.powi(2) + 4.0 * f * d.powi(2),
            2.0 * b * c + 8.0 * f * e * d,
            c.powi(2) + 4.0 * f * e.powi(2) - 4.0 * f * self.minor_radius.powi(2),
        ];

        let roots = solve_quartic(&coeffs);
        let t = roots
            .into_iter()
            .filter(|&t| t >= t_min && t <= t_max)
            .min_by(|a, b| a.partial_cmp(b).unwrap())?;

        let hit_point_object = transformed_ray.point_at_parameter(t);
        let outward_normal_object = self.normal_at(&hit_point_object);

        // Transform hit point and normal back to world space
        let hit_point = self.to_world_space(&hit_point_object);
        let outward_normal = (self.rotation * outward_normal_object).unit();

        let (u, v) = self.get_uv(&hit_point_object);

        Some((
            HitRecord::new(t, hit_point, outward_normal, u, v),
            &*self.material,
        ))
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        let outer_radius = self.major_radius + self.minor_radius;
        let bound = Vec3::new(outer_radius, self.minor_radius, outer_radius);

        // Create an AABB in object space
        let aabb_min = -bound;
        let aabb_max = bound;

        // Transform the AABB to world space
        let corners = [
            self.to_world_space(&Vec3::new(aabb_min.x(), aabb_min.y(), aabb_min.z())),
            self.to_world_space(&Vec3::new(aabb_max.x(), aabb_min.y(), aabb_min.z())),
            self.to_world_space(&Vec3::new(aabb_min.x(), aabb_max.y(), aabb_min.z())),
            self.to_world_space(&Vec3::new(aabb_max.x(), aabb_max.y(), aabb_min.z())),
            self.to_world_space(&Vec3::new(aabb_min.x(), aabb_min.y(), aabb_max.z())),
            self.to_world_space(&Vec3::new(aabb_max.x(), aabb_min.y(), aabb_max.z())),
            self.to_world_space(&Vec3::new(aabb_min.x(), aabb_max.y(), aabb_max.z())),
            self.to_world_space(&Vec3::new(aabb_max.x(), aabb_max.y(), aabb_max.z())),
        ];

        // Find the min and max coordinates of the transformed corners
        let mut min = corners[0];
        let mut max = corners[0];
        for corner in &corners[1..] {
            min = min.min(corner);
            max = max.max(corner);
        }

        Some(AABB::new(min, max))
    }

    fn debug_info(&self) -> String {
        format!(
            "Torus: center = {:?}, major_radius = {}, minor_radius = {}",
            self.center, self.major_radius, self.minor_radius
        )
    }
}

impl Torus {
    fn normal_at(&self, p: &Vec3) -> Vec3 {
        let p_xz = Vec3::new(p.x(), 0.0, p.z());
        let center_to_p_xz = p_xz.unit();
        let q = p_xz + center_to_p_xz * self.major_radius;
        (*p - q).unit()
    }

    fn get_uv(&self, p: &Vec3) -> (f64, f64) {
        let p_xz = Vec3::new(p.x(), 0.0, p.z());
        let phi = p_xz.z().atan2(p_xz.x());
        let theta = (p.y() / self.minor_radius).acos();
        (
            (phi + std::f64::consts::PI) / (2.0 * std::f64::consts::PI),
            theta / std::f64::consts::PI,
        )
    }
}
// Helper function to solve quartic equations
//Newton-Raphson is not the most efficent was was relatively straightforward to implement
// In the future, consider replacing with Ferrari's method or a numerical library that provides root-finding algorithms
fn solve_quartic(coeffs: &[f64; 5]) -> Vec<f64> {
    let mut roots: Vec<f64> = Vec::new();
    let epsilon = 1e-6;
    let max_iterations = 100;

    // Helper function to evaluate the quartic polynomial
    fn evaluate_quartic(coeffs: &[f64; 5], x: f64) -> f64 {
        coeffs[0] * x.powi(4)
            + coeffs[1] * x.powi(3)
            + coeffs[2] * x.powi(2)
            + coeffs[3] * x
            + coeffs[4]
    }

    // Helper function to evaluate the derivative of the quartic polynomial
    fn evaluate_quartic_derivative(coeffs: &[f64; 5], x: f64) -> f64 {
        4.0 * coeffs[0] * x.powi(3) + 3.0 * coeffs[1] * x.powi(2) + 2.0 * coeffs[2] * x + coeffs[3]
    }

    // Newton-Raphson method to find roots
    for initial_guess in [-10.0, -1.0, 0.0, 1.0, 10.0] {
        let mut x = initial_guess;
        for _ in 0..max_iterations {
            let f = evaluate_quartic(coeffs, x);
            let f_prime = evaluate_quartic_derivative(coeffs, x);

            if f_prime.abs() < epsilon {
                break;
            }

            let x_new = x - f / f_prime;
            if (x_new - x).abs() < epsilon {
                if !roots.iter().any(|&root| (root - x_new).abs() < epsilon) {
                    roots.push(x_new);
                }
                break;
            }
            x = x_new;
        }
    }

    roots
}
