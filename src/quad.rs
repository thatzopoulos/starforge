use std::sync::Arc;

use crate::{
    aabb::AABB,
    hittable::{HitRecord, Hittable},
    material::Material,
    ray::Ray,
    vec::Vec3,
};

pub struct Quad {
    pub v0: Vec3,
    pub v1: Vec3,
    pub v2: Vec3,
    pub v3: Vec3,
    pub normal: Vec3,
    pub material: Arc<Material>,
}

// Raytracing The Next Week has this class setup in a way where the check for a point lying inside a quad is factored out so that
// a bunch of other shapes can write their own version of the refactored out is_interior() code and resuse the rest of hte class
// However, I don't think this quad implementation is based on raytracing the next week and I can't seem to find any notes on why I wrote
// it so differently or what I based it on so for now, this will not work and all of my additional shapes like triangles are almost complete rewrites each time.
impl Quad {
    pub fn new(v0: Vec3, v1: Vec3, v2: Vec3, v3: Vec3, material: Arc<Material>) -> Quad {
        let edge1 = v1 - v0;
        let edge2 = v3 - v0;
        let normal = edge1.cross(edge2).unit();

        Quad {
            v0,
            v1,
            v2,
            v3,
            normal,
            material,
        }
    }

    pub fn new_with_normal(
        v0: Vec3,
        v1: Vec3,
        v2: Vec3,
        v3: Vec3,
        normal: Vec3,
        material: Arc<Material>,
    ) -> Quad {
        Quad {
            v0,
            v1,
            v2,
            v3,
            normal,
            material,
        }
    }
}

impl Hittable for Quad {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        // Check if the ray intersects the plane of the quad
        let denom = self.normal.dot(r.direction());
        if denom.abs() < 1e-8 {
            return None; // Ray is parallel to the plane
        }

        let t = (self.v0 - r.origin()).dot(self.normal) / denom;
        if t < t_min || t > t_max {
            return None;
        }

        let p = r.point_at_parameter(t);

        // Check if the intersection point is inside the quad
        let edge1 = self.v1 - self.v0;
        let edge2 = self.v3 - self.v0;
        let hit_vec = p - self.v0;

        let u = hit_vec.dot(edge1) / edge1.squared_length();
        let v = hit_vec.dot(edge2) / edge2.squared_length();

        if u >= 0.0 && u <= 1.0 && v >= 0.0 && v <= 1.0 {
            Some((
                HitRecord {
                    t,
                    p,
                    normal: self.normal,
                    u,
                    v,
                },
                &*self.material,
            ))
        } else {
            None
        }
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        let min_x = self.v0.x.min(self.v1.x).min(self.v2.x).min(self.v3.x);
        let min_y = self.v0.y.min(self.v1.y).min(self.v2.y).min(self.v3.y);
        let min_z = self.v0.z.min(self.v1.z).min(self.v2.z).min(self.v3.z);

        let max_x = self.v0.x.max(self.v1.x).max(self.v2.x).max(self.v3.x);
        let max_y = self.v0.y.max(self.v1.y).max(self.v2.y).max(self.v3.y);
        let max_z = self.v0.z.max(self.v1.z).max(self.v2.z).max(self.v3.z);

        Some(AABB {
            min: Vec3 {
                x: min_x,
                y: min_y,
                z: min_z,
            },
            max: Vec3 {
                x: max_x,
                y: max_y,
                z: max_z,
            },
        })
    }

    fn debug_info(&self) -> String {
        format!(
            "Quad: v0 = {:?}, v1 = {:?}, v2 = {:?}, v3 = {:?}, normal = {:?}",
            self.v0, self.v1, self.v2, self.v3, self.normal
        )
    }
}
