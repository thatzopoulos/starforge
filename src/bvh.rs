use rand::Rng;

use crate::{aabb::*, hittable::*, material::Material};

use std::sync::Arc;

pub struct BVHNode {
    left: Arc<dyn Hittable>,
    right: Arc<dyn Hittable>,
    bbox: AABB,
}

impl BVHNode {
    pub fn new(
        objects: &mut Vec<Arc<dyn Hittable>>,
        start: usize,
        end: usize,
        time0: f64,
        time1: f64,
    ) -> Self {
        let axis = rand::thread_rng().gen_range(0..2);
        let object_span = end - start;
        let left: Arc<dyn Hittable>;
        let right: Arc<dyn Hittable>;

        match object_span {
            1 => {
                left = objects[start].clone();
                right = objects[start].clone();
            }
            2 => match Self::compare(axis, &objects[start], &objects[start + 1]) {
                std::cmp::Ordering::Less => {
                    left = objects[start].clone();
                    right = objects[start + 1].clone();
                }
                _ => {
                    left = objects[start + 1].clone();
                    right = objects[start].clone();
                }
            },
            _ => {
                objects[start..end].sort_unstable_by(|x, y| Self::compare(axis, x, y));
                let mid = start + object_span / 2;
                left = Arc::new(BVHNode::new(objects, start, mid, time0, time1));
                right = Arc::new(BVHNode::new(objects, mid, end, time0, time1));
            }
        }

        let box_left = left.bounding_box(time0, time1);
        let box_right = right.bounding_box(time0, time1);
        box_left
            .and(box_right)
            .expect("No bounding box in BVHNode constructor");

        let bbox = bounding_box(box_left.unwrap(), box_right.unwrap());

        Self { left, right, bbox }
    }

    pub fn new_with_list(list: &mut HittableList, time0: f64, time1: f64) -> Self {
        let length = list.list.len();
        Self::new(&mut list.list, 0, length, time0, time1)
    }

    fn compare(axis: usize, a: &Arc<dyn Hittable>, b: &Arc<dyn Hittable>) -> std::cmp::Ordering {
        let box_a = a.bounding_box(0.0, 0.0);
        let box_b = b.bounding_box(0.0, 0.0);
        box_a
            .and(box_b)
            .expect("No bounding box in BVHNode constructor");

        box_a.unwrap().min()[axis]
            .partial_cmp(&box_b.unwrap().min()[axis])
            .unwrap()
    }
}

impl Hittable for BVHNode {
    fn hit(&self, r: &crate::ray::Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        // First, check the bounding box.
        if !self.bbox.hit(r, t_min, t_max) {
            return None;
        }

        // Check for hit in the left node.
        let hit_left = self.left.hit(r, t_min, t_max);

        // Determine the maximum t for the right node.
        let t_max_right = hit_left
            .as_ref()
            .map_or(t_max, |(hit_rec, _)| hit_rec.t.min(t_max));

        // Check for hit in the right node.
        let hit_right = self.right.hit(r, t_min, t_max_right);

        // Compare hits and return the closest one.
        match (hit_left, hit_right) {
            (Some(left_hit), Some(right_hit)) => {
                if left_hit.0.t < right_hit.0.t {
                    Some(left_hit)
                } else {
                    Some(right_hit)
                }
            }
            (Some(left_hit), None) => Some(left_hit),
            (None, Some(right_hit)) => Some(right_hit),
            (None, None) => None,
        }
    }
    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        Some(self.bbox)
    }
    fn debug_info(&self) -> String {
        let left_info = self.left.debug_info();
        let right_info = self.right.debug_info();
        format!(
            "BVHNode: bbox = {:?}, left = [{}], right = [{}]",
            self.bbox, left_info, right_info
        )
    }
}
