use std::sync::Arc;

use crate::{
    aabb::AABB,
    hittable::{HitRecord, Hittable},
    material::Material,
    ray::Ray,
    vec::Vec3,
};

pub struct Triangle {
    pub v0: Vec3,
    pub v1: Vec3,
    pub v2: Vec3,
    pub normal: Vec3,
    pub material: Arc<Material>,
}

impl Triangle {
    pub fn new(v0: Vec3, v1: Vec3, v2: Vec3, material: Arc<Material>) -> Triangle {
        Triangle {
            v0,
            v1,
            v2,
            normal: (v1 - v0).cross(v2 - v0),
            material,
        }
    }

    pub fn new_with_normal(
        v0: Vec3,
        v1: Vec3,
        v2: Vec3,
        normal: Vec3,
        material: Arc<Material>,
    ) -> Triangle {
        Triangle {
            v0,
            v1,
            v2,
            normal,
            material,
        }
    }
}
impl Hittable for Triangle {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        let v0v1 = self.v1 - self.v0;
        let v0v2 = self.v2 - self.v0;
        let pvec = r.direction.cross(v0v2);
        let det = v0v1.dot(pvec);

        if det.abs() < 1e-4 {
            return None;
        }
        let inv_det = 1. / det;

        let tvec = r.origin - self.v0;
        let u = tvec.dot(pvec) * inv_det;
        if !(0. ..=1.).contains(&u) {
            return None;
        }

        let qvec = tvec.cross(v0v1);
        let v = r.direction.dot(qvec) * inv_det;
        if v < 0. || u + v > 1. {
            return None;
        }

        let t = v0v2.dot(qvec) * inv_det;

        if t < t_min || t > t_max {
            return None;
        }

        let p = r.point_at_parameter(t);

        Some((
            HitRecord {
                u,
                v,
                t,
                p,
                normal: self.normal,
            },
            &*self.material,
        ))
    }

    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        let min_x = self.v0.x.min(self.v1.x).min(self.v2.x);
        let min_y = self.v0.y.min(self.v1.y).min(self.v2.y);
        let min_z = self.v0.z.min(self.v1.z).min(self.v2.z);

        let max_x = self.v0.x.max(self.v1.x).max(self.v2.x);
        let max_y = self.v0.y.max(self.v1.y).max(self.v2.y);
        let max_z = self.v0.z.max(self.v1.z).max(self.v2.z);

        Some(AABB {
            min: Vec3 {
                x: min_x,
                y: min_y,
                z: min_z,
            },
            max: Vec3 {
                x: max_x,
                y: max_y,
                z: max_z,
            },
        })
    }
    fn debug_info(&self) -> String {
        format!(
            "Triangle: v0 = {:?}, v1 = {:?}, v2 = {:?}, normal = {:?}",
            self.v0, self.v1, self.v2, self.normal
        )
    }
}
