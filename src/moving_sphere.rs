use crate::{aabb::*, hittable::*, material::Material, ray::Ray, vec::Vec3};

pub struct MovingSphere {
    center0: Vec3,
    center1: Vec3,
    time0: f64,
    time1: f64,
    radius: f64,
    material: Material,
}

impl MovingSphere {
    pub fn new(
        center0: Vec3,
        center1: Vec3,
        time0: f64,
        time1: f64,
        radius: f64,
        material: Material,
    ) -> Self {
        Self {
            center0,
            center1,
            time0,
            time1,
            radius,
            material,
        }
    }

    pub fn center(&self, time: f64) -> Vec3 {
        self.center0
            + ((time - self.time0) / (self.time1 - self.time0)) * (self.center1 - self.center0)
    }
}
fn get_sphere_uv(p: &Vec3) -> (f64, f64) {
    let theta = (-p.y()).acos();
    let phi = (-p.z()).atan2(p.x()) + std::f64::consts::PI;

    let u = phi / (2.0 * std::f64::consts::PI);
    let v = theta / std::f64::consts::PI;
    (u, v)
}
impl Hittable for MovingSphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        let current_center = self.center(r.time());
        let oc = r.origin() - current_center;
        let a = r.direction().squared_length();
        let half_b = oc.dot(r.direction());
        let c = oc.squared_length() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;

        if discriminant > 0.0 {
            let root = discriminant.sqrt();

            let mut temp = (-half_b - root) / a;
            if temp < t_max && temp > t_min {
                let t = temp;
                let p = r.point_at_parameter(t);
                let outward_normal = (p - current_center) / self.radius;
                let (u, v) = get_sphere_uv(&outward_normal);
                return Some((HitRecord::new(t, p, outward_normal, u, v), &self.material));
            }

            temp = (-half_b + root) / a;
            if temp < t_max && temp > t_min {
                let t = temp;
                let p = r.point_at_parameter(t);
                let outward_normal = (p - current_center) / self.radius;
                let (u, v) = get_sphere_uv(&outward_normal);
                return Some((HitRecord::new(t, p, outward_normal, u, v), &self.material));
            }
        }

        None
    }

    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        let radius = self.radius;
        let radius_vec = Vec3::new(radius, radius, radius);
        let center0 = self.center(t0);
        let center1 = self.center(t1);

        let box0 = AABB::new(center0 - radius_vec, center1 + radius_vec);
        let box1 = AABB::new(center1 - radius_vec, center1 + radius_vec);

        Some(bounding_box(box0, box1))
    }
    fn debug_info(&self) -> String {
        todo!()
    }
}
