use rand::Rng;
use std::sync::Arc;

use crate::hittable::HitRecord;
use crate::ray::Ray;
use crate::texture::{SolidColor, Texture};
use crate::vec::Vec3;

#[derive(Clone)]
pub enum Material {
    Lambertian { texture: Arc<dyn Texture> },
    Metal { attenuation: Vec3, fuzziness: f64 },
    Dielectric { refraction: f64 },
}

fn random_in_unit_sphere() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p = Vec3::new(
            rng.gen_range(-1.0..1.0),
            rng.gen_range(-1.0..1.0),
            rng.gen_range(-1.0..1.0),
        );
        if p.squared_length() < 1.0 {
            return p;
        }
    }
}

fn reflect(v: Vec3, n: Vec3) -> Vec3 {
    v - n * v.dot(n) * 2.0
}

fn refract(v: Vec3, n: Vec3, ni_over_nt: f64) -> Option<Vec3> {
    let uv = v.unit();
    let dt = uv.dot(n);
    let discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt * dt);
    if discriminant > 0.0 {
        Some((uv - n * dt) * ni_over_nt - n * discriminant.sqrt())
    } else {
        None
    }
}

fn schlick(cosine: f64, ref_idx: f64) -> f64 {
    let r0 = ((1.0 - ref_idx) / (1.0 + ref_idx)).powi(2);
    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}

impl Material {
    pub fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Ray, Vec3)> {
        match self {
            Material::Lambertian { texture } => {
                let scatter_direction = rec.normal() + random_in_unit_sphere().unit();
                let scattered = Ray::new(rec.p(), scatter_direction, r_in.time());
                let attenuation = texture.value(rec.u, rec.v, rec.p());
                Some((scattered, attenuation))
            }
            Material::Metal {
                attenuation,
                fuzziness,
            } => {
                let reflected = reflect(r_in.direction().unit(), rec.normal());
                let scattered = Ray::new(
                    rec.p(),
                    reflected + random_in_unit_sphere() * *fuzziness,
                    r_in.time(),
                );
                if scattered.direction().dot(rec.normal()) > 0.0 {
                    Some((scattered, *attenuation))
                } else {
                    None
                }
            }
            Material::Dielectric { refraction } => {
                let attenuation = Vec3::new(1.0, 1.0, 1.0);
                let (outward_normal, ni_over_nt, cosine) =
                    if r_in.direction().dot(rec.normal()) > 0.0 {
                        (
                            -rec.normal(),
                            *refraction,
                            r_in.direction().dot(rec.normal()) * *refraction
                                / r_in.direction().length(),
                        )
                    } else {
                        (
                            rec.normal(),
                            1.0 / *refraction,
                            -r_in.direction().dot(rec.normal()) / r_in.direction().length(),
                        )
                    };

                if let Some(refracted) = refract(r_in.direction(), outward_normal, ni_over_nt) {
                    let reflect_prob = schlick(cosine, *refraction);
                    if rand::random::<f64>() < reflect_prob {
                        let reflected = reflect(r_in.direction(), rec.normal());
                        Some((Ray::new(rec.p(), reflected, r_in.time()), attenuation))
                    } else {
                        Some((Ray::new(rec.p(), refracted, r_in.time()), attenuation))
                    }
                } else {
                    let reflected = reflect(r_in.direction(), rec.normal());
                    Some((Ray::new(rec.p(), reflected, r_in.time()), attenuation))
                }
            }
        }
    }
}

pub fn create_lambertian(color: Vec3) -> Material {
    Material::Lambertian {
        texture: Arc::new(SolidColor::from_vec3(color)),
    }
}
