use crate::vec::Vec3;
use rand::Rng;

pub struct Perlin {
    randvec: [Vec3; Self::POINT_COUNT],
    perm_x: [i32; Self::POINT_COUNT],
    perm_y: [i32; Self::POINT_COUNT],
    perm_z: [i32; Self::POINT_COUNT],
}

impl Perlin {
    const POINT_COUNT: usize = 256;

    pub fn new() -> Self {
        let mut rng = rand::thread_rng();
        let randvec: [Vec3; Self::POINT_COUNT] = std::array::from_fn(|_| {
            Vec3::new(
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
            )
            .unit()
        });

        let perm_x = Self::perlin_generate_perm();
        let perm_y = Self::perlin_generate_perm();
        let perm_z = Self::perlin_generate_perm();

        Self {
            randvec,
            perm_x,
            perm_y,
            perm_z,
        }
    }
    pub fn noise(&self, p: Vec3) -> f64 {
        let u = p.x - p.x.floor();
        let v = p.y - p.y.floor();
        let w = p.z - p.z.floor();

        let i = p.x.floor() as i32;
        let j = p.y.floor() as i32;
        let k = p.z.floor() as i32;

        let mut c = [[[Vec3::new(0.0, 0.0, 0.0); 2]; 2]; 2];

        for di in 0..2 {
            for dj in 0..2 {
                for dk in 0..2 {
                    let index = (self.perm_x[((i + di as i32) & 255) as usize]
                        ^ self.perm_y[((j + dj as i32) & 255) as usize]
                        ^ self.perm_z[((k + dk as i32) & 255) as usize])
                        as usize;
                    c[di][dj][dk] = self.randvec[index];
                }
            }
        }

        Self::perlin_interp(c, u, v, w)
    }
    fn perlin_interp(c: [[[Vec3; 2]; 2]; 2], u: f64, v: f64, w: f64) -> f64 {
        let mut accum = 0.0;
        for i in 0..2 {
            for j in 0..2 {
                for k in 0..2 {
                    let weight_v = Vec3::new(u - i as f64, v - j as f64, w - k as f64);
                    accum += (i as f64 * u + (1 - i) as f64 * (1.0 - u))
                        * (j as f64 * v + (1 - j) as f64 * (1.0 - v))
                        * (k as f64 * w + (1 - k) as f64 * (1.0 - w))
                        * c[i][j][k].dot(weight_v);
                }
            }
        }
        accum
    }

    fn perlin_generate_perm() -> [i32; Self::POINT_COUNT] {
        let mut p: [i32; Self::POINT_COUNT] = (0..Self::POINT_COUNT as i32)
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();
        Self::permute(&mut p);
        p
    }

    fn permute(p: &mut [i32]) {
        let mut rng = rand::thread_rng();
        for i in (1..p.len()).rev() {
            let target = rng.gen_range(0..=i);
            p.swap(i, target);
        }
    }
    pub fn turb(&self, p: Vec3, depth: i32) -> f64 {
        let mut accum = 0.0;
        let mut temp_p = p;
        let mut weight = 1.0;

        for _ in 0..depth {
            accum += weight * self.noise(temp_p);
            weight *= 0.5;
            temp_p = temp_p * 2.0;
        }

        accum.abs()
    }
}
