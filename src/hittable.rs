extern crate tobj;

use std::f64::consts::PI;
use std::path::Path;
use std::sync::Arc;

use crate::aabb::AABB;
use crate::material::Material;
use crate::ray::Ray;
use crate::triangle::Triangle;
use crate::vec::Vec3;
use tobj::LoadOptions;

pub struct HitRecord {
    pub t: f64,
    pub p: Vec3,
    pub normal: Vec3,
    pub u: f64,
    pub v: f64,
}

impl HitRecord {
    pub fn new(t: f64, p: Vec3, normal: Vec3, u: f64, v: f64) -> HitRecord {
        HitRecord { t, p, normal, u, v }
    }

    pub fn t(&self) -> f64 {
        self.t
    }

    pub fn p(&self) -> Vec3 {
        self.p
    }

    pub fn normal(&self) -> Vec3 {
        self.normal
    }
}

pub trait Hittable: Sync + Send {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)>;
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB>;
    fn debug_info(&self) -> String;
}

pub struct Sphere {
    center: Vec3,
    radius: f64,
    material: Material,
}

impl Sphere {
    pub fn new(center: Vec3, radius: f64, material: Material) -> Sphere {
        Sphere {
            center,
            radius,
            material,
        }
    }

    pub fn center(&self) -> Vec3 {
        self.center
    }

    pub fn radius(&self) -> f64 {
        self.radius
    }

    pub fn material(&self) -> &Material {
        &self.material
    }
}
fn get_sphere_uv(p: &Vec3) -> (f64, f64) {
    // p: a given point on the sphere of radius one, centered at the origin.
    // returns (u, v) where:
    // u: returned value [0,1] of angle around the Y axis from X=-1.
    // v: returned value [0,1] of angle from Y=-1 to Y=+1.
    //     <1 0 0> yields <0.50 0.50>       <-1  0  0> yields <0.00 0.50>
    //     <0 1 0> yields <0.50 1.00>       < 0 -1  0> yields <0.50 0.00>
    //     <0 0 1> yields <0.25 0.50>       < 0  0 -1> yields <0.75 0.50>

    let theta = (-p.y).acos();
    let phi = (-p.z).atan2(p.x) + PI;

    let u = phi / (2.0 * PI);
    let v = theta / PI;

    (u, v)
}
impl Hittable for Sphere {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        let oc = r.origin() - self.center();
        let a = r.direction().dot(r.direction());
        let b = oc.dot(r.direction());
        let c = oc.dot(oc) - self.radius() * self.radius();
        let discriminant = b * b - a * c;
        if discriminant > 0.0 {
            let t1 = (-b - discriminant.sqrt()) / a;
            let t2 = (-b + discriminant.sqrt()) / a;
            if t1 < t_max && t1 > t_min {
                let p = r.point_at_parameter(t1);
                let n = (p - self.center()) / self.radius();
                let (u, v) = get_sphere_uv(&n);
                Some((HitRecord::new(t1, p, n, u, v), self.material()))
            } else if t2 < t_max && t2 > t_min {
                let p = r.point_at_parameter(t2);
                let n = (p - self.center()) / self.radius();
                let (u, v) = get_sphere_uv(&n);
                Some((HitRecord::new(t2, p, n, u, v), self.material()))
            } else {
                None
            }
        } else {
            None
        }
    }
    fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
        let radius = self.radius;
        let radius_vec = Vec3::new(radius, radius, radius);
        Some(AABB::new(
            self.center - radius_vec,
            self.center + radius_vec,
        ))
    }
    fn debug_info(&self) -> String {
        format!(
            "Sphere: center = {:?}, radius = {}",
            self.center, self.radius
        )
    }
}

use crate::texture::SolidColor;

pub fn obj_to_hittable(path: &Path) -> HittableList {
    let obj = self::tobj::load_obj(
        path,
        &LoadOptions {
            ..Default::default()
        },
    );
    let (models, mtls) = obj.unwrap();
    let mut world: Vec<Arc<dyn Hittable>> = Vec::new();

    let default_mat: Arc<Material> = Arc::new(Material::Lambertian {
        texture: Arc::new(SolidColor::new(0.6, 0.6, 0.6)),
    });
    let materials: Vec<Arc<Material>> = mtls
        .unwrap()
        .iter()
        .map(|m| {
            let mat: Arc<Material> = Arc::new(Material::Lambertian {
                texture: Arc::new(SolidColor::new(
                    m.diffuse[0].into(),
                    m.diffuse[1].into(),
                    m.diffuse[2].into(),
                )),
            });

            mat
        })
        .collect();
    for m in models.iter() {
        let mesh = &m.mesh;
        for f in 0..mesh.indices.len() / 3 {
            let i0 = mesh.indices[3 * f] as usize;
            let i1 = mesh.indices[3 * f + 1] as usize;
            let i2 = mesh.indices[3 * f + 2] as usize;
            let v0 = Vec3::new(
                mesh.positions[i0 * 3].into(),
                mesh.positions[i0 * 3 + 1].into(),
                mesh.positions[i0 * 3 + 2].into(),
            );
            let v1 = Vec3::new(
                mesh.positions[i1 * 3].into(),
                mesh.positions[i1 * 3 + 1].into(),
                mesh.positions[i1 * 3 + 2].into(),
            );
            let v2 = Vec3::new(
                mesh.positions[i2 * 3].into(),
                mesh.positions[i2 * 3 + 1].into(),
                mesh.positions[i2 * 3 + 2].into(),
            );

            let mat: Arc<Material> = match mesh.material_id {
                Some(id) => Arc::clone(&materials[id]),
                None => Arc::clone(&default_mat),
            };

            let tri: Triangle;
            if !mesh.normals.is_empty() {
                let normal = Vec3::new(
                    mesh.normals[i0 * 3].into(),
                    mesh.normals[i0 * 3 + 1].into(),
                    mesh.normals[i0 * 3 + 2].into(),
                );
                tri = Triangle::new_with_normal(v0, v1, v2, normal, mat)
            } else {
                tri = Triangle::new(v0, v1, v2, mat);
            }

            world.push(Arc::new(tri));
        }
    }
    HittableList::new(world)
}

#[derive(Clone)]
pub struct HittableList {
    pub list: Vec<Arc<dyn Hittable>>,
}

impl HittableList {
    pub fn new(list: Vec<Arc<dyn Hittable>>) -> HittableList {
        HittableList { list }
    }
}

impl Hittable for HittableList {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<(HitRecord, &Material)> {
        let mut closest_so_far = t_max;
        let mut res = None;
        for h in self.list.iter() {
            if let Some((hit_record, material)) = h.hit(r, t_min, closest_so_far) {
                closest_so_far = hit_record.t();
                res = Some((
                    HitRecord::new(
                        hit_record.t(),
                        hit_record.p(),
                        hit_record.normal(),
                        hit_record.u,
                        hit_record.v,
                    ),
                    material,
                ))
            }
        }
        res
    }
    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        let first = self.list.first().and_then(|x| x.bounding_box(t0, t1));
        first?;

        self.list.iter().skip(1).fold(first, |acc, x| {
            x.bounding_box(t0, t1)
                .map(|bounding_box| crate::aabb::bounding_box(acc.unwrap(), bounding_box))
        })
    }
    fn debug_info(&self) -> String {
        let num_hittables = self.list.len();
        let hittable_info: Vec<String> = self.list.iter().map(|h| h.debug_info()).collect();

        format!(
            "HittableList: num_hittables = {}\n{}",
            num_hittables,
            hittable_info.join("\n")
        )
    }
}
