# Starforge Rust Raytracer

![Example Gif Render](hqrenders/800x600-5-samples.gif)

Starforge is a Rust implementation of the raytracer from Raytracing In A Weekend. It is based on the raytracer from Book 1 with some
changes and improvements.

I have added a parser for command line arguments, parallelized the code using Rayon, and added a `--gif` flag that will 
spin the camera 360 degrees around the Scene in 100 steps, raytracing an image for each step that can be combined into a gif
using Imagemagick.

## Example Usage

### Gif Creation
The following command (assuming you have imagemagick installed on your machine), will create a low quality (1 sample per
pixel) gif of the size 400x300 pixels named test.gif.

```bash
mkdir gif/ # You need somewhere to write out the 100 png files used to create the gif
cargo run --release -- --gif --width 400 --height 300 --samples 1 --output output && convert -delay 10 -loop 0 gif/output-{0..99}.png test.gif
```

### Single Image Creation
The following command will create a single image of 400x300 pixels with 10 samples per pixel named example.png
```bash
cargo run --release -- --width 800 --height 600 --samples 10 --output example
```

## Future Improvements
 - Move gif creation from imagemagick to the raytracer itself
 - Ability to load .obj files
 - Implement Books 2 and 3 of Raytracing In A Weekend
 - Figure out how to get loading bars working when raytracing a single image. Currently indicatif does not play well with the rayon par_iter that calculates everything

## Sample Renders
Below are some of the renders from this raytracer. Additional images and gifs can be found in the hqrenders/ folder.

![HQ Single Image Render](hqrenders/3840x2160-20k-samples.png)

![WIP Gif Render](hqrenders/800x600-50-samples.gif)
